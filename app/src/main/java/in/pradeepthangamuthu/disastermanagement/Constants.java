package in.pradeepthangamuthu.disastermanagement;

/**
 * Created by pradeepthangamuthu on 03/08/18.
 */

public class Constants {
        public static final int DISASTER_TSUNAMI = 1;
        public static final int DISASTER_FLOOD = 2;
        public static final int DISASTER_FIRE = 3;
        public static final int DISASTER_EARTHQUAKE = 4;
}
