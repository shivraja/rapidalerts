package in.pradeepthangamuthu.disastermanagement.firebase;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import in.pradeepthangamuthu.disastermanagement.Dashboard;
import in.pradeepthangamuthu.disastermanagement.R;
import in.pradeepthangamuthu.disastermanagement.contentprovider.DisasterDataProvider;
import in.pradeepthangamuthu.disastermanagement.models.Disaster;

import static android.content.ContentValues.TAG;

/**
 * Created by pradeepthangamuthu on 02/08/18.
 */

public class FCMMessagingService extends FirebaseMessagingService {

    private static String CHANNEL_ID = "disasterChannel1";
    private static final String TAG = "DMS_FCM";
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // ...

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.d(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + remoteMessage.getData());

            processMessage(remoteMessage.getData());

        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }

    private void processMessage(Map<String, String> data){
    try {
        String postalCode = data.get("postalcode");
        double latitude = Double.parseDouble(data.get("latitude"));
        double longitude = Double.parseDouble(data.get("longitude"));
        String location = data.get("location");
        int disasterType = Integer.parseInt(data.get("disasterType"));
        if(latitude>=0 && longitude>=0 && disasterType>=1){
            Disaster disaster = new Disaster(postalCode,location,disasterType,latitude,longitude);
            showNotification(disaster);
            DisasterDataProvider disasterDataProvider = DisasterDataProvider.getObject(this.getApplicationContext());
            disasterDataProvider.addNewDisaster(disaster);
        }
    }catch (Exception e){
        Log.d(TAG, "processMessage: Error in processing Message");
    }

        Log.d(TAG, "showNotification: "+data.toString());
    }

    private void showNotification(Disaster disaster){

        Intent resultIntent = new Intent(this, Dashboard.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(Dashboard.class);

        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this.getApplicationContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_background)
                .setContentTitle("Alert!!!")
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setLights(Color.RED, 3000, 3000)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentText("New disaster in "+disaster.location)
                .setContentIntent(resultPendingIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        NotificationManager mNotificationManager = (NotificationManager) this.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

// notificationID allows you to update the notification later on.
        mNotificationManager.notify(188087, mBuilder.build());

    }

}
