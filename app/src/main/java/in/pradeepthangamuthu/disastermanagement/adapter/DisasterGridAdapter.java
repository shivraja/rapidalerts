package in.pradeepthangamuthu.disastermanagement.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.pradeepthangamuthu.disastermanagement.Constants;
import in.pradeepthangamuthu.disastermanagement.R;
import in.pradeepthangamuthu.disastermanagement.models.Disaster;

/**
 * Created by pradeepthangamuthu on 03/08/18.
 */

public class DisasterGridAdapter extends BaseAdapter {

    private static final String TAG = "DISATER_GV_ADTR";
    private List<Disaster> disasterList;
    private Context context;
    public DisasterGridAdapter(Context context, List<Disaster> disasterList){
        this.disasterList = disasterList;
        this.context = context;
    }
    @Override
    public int getCount() {
        return disasterList.size();
    }

    @Override
    public Object getItem(int position) {
        return disasterList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_disaster, parent, false);
        }

        Disaster disaster = disasterList.get(position);

        ImageView disasterImage = convertView.findViewById(R.id.disasterImage);
        TextView disasterText = convertView.findViewById(R.id.disasterText);
        if (disaster != null) {
            disasterText.setText(disaster.location);
            Log.d(TAG, "getView: "+disaster.disasterType);
            switch (disaster.disasterType){
                case Constants.DISASTER_EARTHQUAKE:
                    disasterImage.setBackground(ContextCompat.getDrawable(context, R.drawable.earthquake));
                    break;
                case Constants.DISASTER_FIRE:
                    disasterImage.setBackground(ContextCompat.getDrawable(context, R.drawable.volcano));
                    break;

                case Constants.DISASTER_FLOOD:
                    disasterImage.setBackground(ContextCompat.getDrawable(context, R.drawable.tornado));
                    break;

                case Constants.DISASTER_TSUNAMI:
                    disasterImage.setBackground(ContextCompat.getDrawable(context, R.drawable.tsunami2));
                    break;

                default:
                    disasterImage.setBackground(ContextCompat.getDrawable(context, R.drawable.tsunami2));
                    break;
            }
        }
        return convertView;
    }
}
