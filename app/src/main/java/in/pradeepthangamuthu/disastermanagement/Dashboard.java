package in.pradeepthangamuthu.disastermanagement;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.pradeepthangamuthu.disastermanagement.adapter.DisasterGridAdapter;
import in.pradeepthangamuthu.disastermanagement.contentprovider.DisasterDataProvider;

public class Dashboard extends AppCompatActivity {

    private static final String TAG = "DISASTER_DASH";
    private DisasterDataProvider disasterDataProvider;
    public static Address address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getLocation();
        disasterDataProvider = DisasterDataProvider.getObject(this);

        ImageView mapView = findViewById(R.id.mapImageView);
        mapView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMapPage();
            }
        });

        DisasterGridAdapter gridAdapter = new DisasterGridAdapter(this, disasterDataProvider.getAllDisasterFromDb());
        GridView  disasterGrid = findViewById(R.id.disasterGridView);
        disasterGrid.setAdapter(gridAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showAlertCreatePage();
            }
        });
    }

    private void showAlertCreatePage(){
        Intent mapIntent = new Intent(this, CreateDisasterActivity.class);
        startActivity(mapIntent);
    }

    private void showMapPage(){
        Intent mapIntent = new Intent(this, MapsActivity.class);
        startActivity(mapIntent);
    }

    private void subscribe(String postalCode){
        FirebaseMessaging.getInstance().subscribeToTopic("POST"+postalCode);
        Log.d(TAG, "subscribe: "+postalCode);
    }

    private void unsubscribe(String postalCode){
        FirebaseMessaging.getInstance().subscribeToTopic("POST"+postalCode);
        Log.d(TAG, "unsubscribe: "+postalCode);
    }


    private void getLocation(){
        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);
        if ( Build.VERSION.SDK_INT >= 23 &&

                ContextCompat.checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, "Location Permission not available 2", Toast.LENGTH_SHORT).show();
            return;
        }

        client.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                location.getLatitude();
                location.getLongitude();
                getAddress(location.getLatitude(),location.getLongitude());
            }
        });
    }

    public void getAddress(double lat, double lng) {
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 5);

            if (addresses != null && addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {
                    Address obj = addresses.get(i);
                    String add = obj.getAddressLine(0);
                    add = add + "\n" + obj.getCountryName();
                    add = add + "\n" + obj.getCountryCode();
                    add = add + "\n" + obj.getAdminArea();
                    add = add + "\n" + obj.getPostalCode();
                    add = add + "\n" + obj.getSubAdminArea();
                    add = add + "\n" + obj.getLocality();
                    add = add + "\n" + obj.getSubThoroughfare();

                    Log.v("IGA", "Address" + add);
                    if(obj.getSubAdminArea()!=null && obj.getPostalCode()!=null){
                        address = obj;
                        refreshSubscription(obj.getPostalCode());
                        break;
                    }
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void refreshSubscription(String postalCode){
        Log.d(TAG, "refreshSubscription: "+postalCode);
        if(!postalCode.isEmpty()) {
            postalCode = postalCode.trim();
            String savedCode = getState().trim();
            if (!postalCode.matches(savedCode)) {
                unsubscribe(savedCode);
                saveState(postalCode);
            }
            subscribe(postalCode);
        }
    }

    protected void saveState(String location){
        SharedPreferences sharedPreferences = this.getSharedPreferences("DisasterSP",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("USER_LOC", location);
        Log.d("", "saveState: "+location);
        editor.commit();
    }

    protected String getState(){
        SharedPreferences sharedPreferences = this.getSharedPreferences("DisasterSP",MODE_PRIVATE);
        String loc = sharedPreferences.getString("USER_LOC","");
        Log.d(TAG, "getState: "+loc);
        return loc;
    }
}