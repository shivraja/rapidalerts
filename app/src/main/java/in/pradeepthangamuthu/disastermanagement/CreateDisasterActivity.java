package in.pradeepthangamuthu.disastermanagement;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.RemoteMessage;;import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import in.pradeepthangamuthu.disastermanagement.models.Disaster;

public class CreateDisasterActivity extends AppCompatActivity {

    private static int selectedAlert = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_disaster);

        Spinner spinner = findViewById(R.id.disasterType);
        Button button = findViewById(R.id.createAlertButton);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
        // Spinner Drop down elements

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedAlert = position+1;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest();
            }
        });
    }

    private void sendRequest(){
        if(Dashboard.address!=null){
            double latitude = Dashboard.address.getLatitude();
            double longitude = Dashboard.address.getLongitude();
            String location = Dashboard.address.getSubAdminArea();
            location = location.isEmpty() ? "Unknown" : location;
            String postalCode = Dashboard.address.getPostalCode();
            Disaster disaster = new Disaster(postalCode,location,selectedAlert,latitude,longitude);
            updateServer(disaster);
        }else{
            Toast.makeText(this, "Unable To Process Alert", Toast.LENGTH_SHORT).show();
        }
    }

    private void updateServer(Disaster disaster) {

        class UpdateDisaster extends AsyncTask<String, Void, String> {

            @Override
            protected String doInBackground(String... strings) {
                String postalCode, location, disasterType, latitude, longitude;
                postalCode = strings[0];
                location = strings[1];
                disasterType = strings[2];
                latitude = strings[3];
                longitude = strings[4];

                try {

                    URL url = new URL("http://pradeepthangamuthu.in/DISASTER/send_notifications.php");
                    Log.d("SMART_SERVICE", url.toString());
                    URLConnection connection = url.openConnection();
                    connection.setDoOutput(true);

                    String data = URLEncoder.encode("postalcode", "UTF-8")
                            + "=" + URLEncoder.encode(postalCode, "UTF-8");

                    data += "&" + URLEncoder.encode("location", "UTF-8") + "="
                            + URLEncoder.encode(location, "UTF-8");

                    data += "&" + URLEncoder.encode("disastertype", "UTF-8")
                            + "=" + URLEncoder.encode(disasterType, "UTF-8");

                    data += "&" + URLEncoder.encode("latitude", "UTF-8")
                            + "=" + URLEncoder.encode(latitude, "UTF-8");

                    data += "&" + URLEncoder.encode("longitude", "UTF-8")
                            + "=" + URLEncoder.encode(longitude, "UTF-8");

                    OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
                    wr.write(data);
                    wr.flush();

                    InputStream inputStream = connection.getInputStream();
                    InputStreamReader inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
                    BufferedReader reader = new BufferedReader(inputStreamReader);

                    String line;
                    String pre = "";
                    while ((line = reader.readLine()) != null) {
                        Log.d("SMART_SERVICE", line);
                        pre = line;
                    }
                    pre = pre.trim();
                    Log.d("SMART_SERVICE", "PRE " + pre+" "+pre.endsWith("success")+" "+pre.contains("success"));
                    if (pre.endsWith("success"))
                        return "success";

                } catch (MalformedURLException e) {
                    Log.d("SMART_SERVICE", e.getMessage());
                } catch (IOException a) {
                    Log.d("SMART_SERVICE", a.getMessage());
                }
               // Toast.makeText(CreateDisasterActivity.this, "FAILED", Toast.LENGTH_SHORT).show();
                return "FAILED";
            }

            @Override
            protected void onPostExecute(String result) {
                if (result.matches("success")) {
                    Log.w("DISASTER","Reached Post Success");
                }
                finish();
            }
        }
        new UpdateDisaster().execute(disaster.postalCode, disaster.location, disaster.disasterType+"", disaster.latitude+"" ,disaster.longitude+"");
    }
}

//
//package com.android.shiv.smartclass.Services;
//
//        import android.app.Service;
//        import android.content.Context;
//        import android.content.Intent;
//        import android.net.ConnectivityManager;
//        import android.os.AsyncTask;
//        import android.os.IBinder;
//        import android.support.annotation.Nullable;
//        import android.util.Log;
//
//        import com.android.shiv.smartclass.DataProviders.Student.CompletedAssignment;
//        import com.android.shiv.smartclass.DataProviders.Student.DataProvider;
//
//        import java.io.BufferedReader;
//        import java.io.IOException;
//        import java.io.InputStream;
//        import java.io.InputStreamReader;
//        import java.io.OutputStreamWriter;
//        import java.net.MalformedURLException;
//        import java.net.URL;
//        import java.net.URLConnection;
//        import java.net.URLEncoder;
//
///**
// * Created by Shiv on 26-Jun-16.
// */
//
//public class NotifyAssignmentCompletion extends Service {
//
//    DataProvider dataProvider;
//    final static String TAG = "SMART_SERVICE";
//
//    @Nullable
//    @Override
//    public IBinder onBind(Intent intent) {
//        return null;
//    }
//
//    @Override
//    public void onCreate() {
//        super.onCreate();
//        dataProvider = new DataProvider(getBaseContext());
//        ConnectivityManager cm = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
//        if (cm == null)
//            return;
//        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected()) {
//            for (CompletedAssignment assignment : dataProvider.getCompletedAssignments()) {
//                updateServer(assignment);
//            }
//        } else {
//            // Do nothing or notify user somehow
//        }
//    }
//
//
//}

