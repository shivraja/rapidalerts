package in.pradeepthangamuthu.disastermanagement.models;

/**
 * Created by pradeepthangamuthu on 03/08/18.
 */

public class Disaster {
    public int disasterType;
    public String location;
    public String postalCode;
    public double latitude;
    public double longitude;

    public Disaster(String postalCode, String location, int disasterType, double latitude, double longitude){
        this.postalCode = postalCode;
        this.location = location;
        this.latitude = latitude;
        this.longitude = longitude;
        this.disasterType = disasterType;
    }

}