package in.pradeepthangamuthu.disastermanagement.contentprovider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import in.pradeepthangamuthu.disastermanagement.models.Disaster;

/**
 * Created by pradeepthangamuthu on 03/08/18.
 */

public class DisasterDataProvider {

    private static DisasterDataProvider dataProvider;
    private DbHandler dbHandler;
    private Context context;
    List<Disaster> disasterList;
    public static DisasterDataProvider getObject(Context context){
        if(dataProvider!=null)
            return dataProvider;
        dataProvider = new DisasterDataProvider(context);
        return dataProvider;
    }

    private DisasterDataProvider(Context context){
        this.context = context;
        this.dbHandler = DbHandler.getInstance(context);
    }

    public List<Disaster> getAllDisaster(){
        List<Disaster> disasterList = new ArrayList<>();
        disasterList.add(new Disaster("Text1","T1", 1,-10, 30));
        disasterList.add(new Disaster("Text2","T2", 2,-20, 40));
        disasterList.add(new Disaster("Text3","T3", 3,-30, 40));
        disasterList.add(new Disaster("Text4","T4", 4,-40, 60));
        disasterList.add(new Disaster("Text5","T5", 5,-50, 70));
        return  disasterList;
    }

    public List<Disaster> getAllDisasterFromDb(){
        if(disasterList!=null)
            return disasterList;

        SQLiteDatabase db = dbHandler.getReadableDatabase();

        List<Disaster> disasterList = new ArrayList<>();
        String query = "SELECT * FROM " + DbHandler.DISASTER_TABLE+ " ORDER BY Time DESC;";
        Cursor cursor = db.rawQuery(query, null);

        String location, postalcode;
        double latitude, longitude;
        int disasterType;

        if (cursor.moveToFirst()) {
            do {
                location = cursor.getString(cursor.getColumnIndex("Location"));
                postalcode = cursor.getString(cursor.getColumnIndex("PostalCode"));
                latitude = cursor.getDouble(cursor.getColumnIndex("Latitude"));
                longitude = cursor.getDouble(cursor.getColumnIndex("Longitude"));
                disasterType = cursor.getInt(cursor.getColumnIndex("DisasterType"));
                Disaster disaster = new Disaster(postalcode,location,disasterType, latitude,longitude);

                disasterList.add(disaster);
            } while (cursor.moveToNext());
        }
        db.close();
        return disasterList;
    }

    public boolean addNewDisaster(Disaster disaster){
        disasterList = null;

        SQLiteDatabase db = dbHandler.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put("Location",disaster.location );
        contentValues.put("PostalCode",disaster.postalCode );
        contentValues.put("Latitude",disaster.latitude );
        contentValues.put("Longitude",disaster.longitude );
        contentValues.put("DisasterType",disaster.disasterType );

        boolean result =  db.insert(DbHandler.DISASTER_TABLE, null, contentValues) != -1;
        db.close();
        return result;
    }
}
