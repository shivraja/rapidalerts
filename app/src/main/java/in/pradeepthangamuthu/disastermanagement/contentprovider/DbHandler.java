package in.pradeepthangamuthu.disastermanagement.contentprovider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by pradeepthangamuthu on 03/08/18.
 */

public class DbHandler  extends SQLiteOpenHelper {

    final static String TAG = "DISASTER_DB_HANDLER";
    final static String DATABASE_NAME = "disaster.db";
    final static int DATABASE_VERSION = 1;

    final static String DISASTER_TABLE = "Brand";
    private static DbHandler sInstance;
    public static synchronized DbHandler getInstance(Context context) {

        // Use the application context, which will ensure that you
        // don't accidentally leak an Activity's context.
        // See this article for more information: http://bit.ly/6LRzfx
        if (sInstance == null) {
            sInstance = new DbHandler(context.getApplicationContext());
        }
        return sInstance;
    }

    private DbHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DISASTER_TABLE + " ( "
                + "Location VARCHAR(200)" + ","
                + "PostalCode VARCHAR(20)" + ","
                + "DisasterType INTEGER" + ","
                + "Latitude REAL" + ","
                + "Longitude REAL" + ","
                + "Time TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DISASTER_TABLE);
        onCreate(db);
    }
}